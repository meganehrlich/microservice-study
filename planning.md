Finish the RESTful
    - finish adding decorators to view functions

 In events views:
* [x] - api_show_conference
    ["GET", "PUT", "DELETE"]

 In attendees views:
* [x] - api_show_attendee
    ["GET", "PUT", "DELETE"]

 In presentation views:
* [x] - api_list_presentations
    ["GET", "POST"]
    default value for status on new presentations should be "SUBMITTED"
    instead of: presentation = Presentation.objects.create(**content)
    try: presentation = Presentation.create(**content)
    ```````````````````````````````````````````````````````````````````
        @classmethod
        def create(cls, **kwargs):
            kwargs["status"] = Status.objects.get(name="SUBMITTED")
            presentation = cls(**kwargs)
            presentation.save()
            return presentation
    ```````````````````````````````````````````````````````````````````
[x] api_show_presentations
    ["GET", "PUT", "DELETE"]





W7D4
* [x] install requests
* [x] pip freeze requirements.txt


* [x] Update for when you create Location to get url to a picture of that city
  
* [x] add acls.py file in events app
  * [x] add code that will make the http requests to pexel and weather
  * [x] at top of acls.py import:
        from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
        json
        requests

    use pexels using api key
  * [x] will need header for request
      * [x] Update Location model(?) to include "picture_url" 
        * [x] None/null for existing
        * [x] run migrations
      * [x] create function in acls.py 
        * [x] accepts parameters (city, state)
        * [x] header name = "Authorization"
        * [x] header value = "{API key}"
        * [x] create params dictionary with per_page and query
        * [x] add url
        * [x] define response
        * [x] define content
        * [x] try - return pphoto
        * [x] except - return None
      * [x] update encoder in views



      * https://learn-2.galvanize.com/cohorts/3352/blocks/1873/content_files/build/04-hello-acls/66-integrating-third-party-data.md#:~:text=request.%20Here%27s%20a-,link%20to%20the%20example,-that%20shows%20how
        * instead of "user-agent" will be "Authorization"
  * To use API key:
  * [x] in .gitignore file add events/keys.py near top
  * [x] create events/keys.py file and put pexel and weather api keys in

 

* [x] Update Conference details to get weather data for city
    use Geocoding API to convert city to Latitude and Longitude
    Find current weather data for latitude longitude using above
    * [x] add temp and description to content
    * [x] if no data "weather": null,

    * [x] create function in acls.py

* [x] Add Dockerfile
* [x] Add Dockerfile.dev

* [x] Change the code structure for attendees to be a microservice
* [x] Create value objects to reference entities in other bounded contexts
* [x] Create a cron job in a microservice to poll another microservice
* [x] Start two Docker containers on a user-defined bridge network to show them working together

Unomonolith with Queues
* [] Create endpoint for PUT request approve or reject presentation
  * [] an email will be sent after decision made

* [x] Pull MailHog images from Docker Hub
* [ ] Update code in presentations to turn them into producers
* [ ] Create new django project
  * [ ] Create consumer for RabbitMQ
  
* [x] Add pika dependancy to monolith/requirements.txt
* [x] Rebuild development image
* [x] In monolith/presentations/api_views.py:
  * Add approve function
  * Add reject function
  * Add method get_extra_data to PresentationDetailEncoder
* [x] In monolith/presetnations/api_urls:
  * import new functions
  * add paths for these functions
* [x] Create superuser and check new functions
  
* [x] Get lost in a rabbit hole of darkness and despair
* [x] Start changing things in many places with no particular plan
* [x] Somehow get things running!
* [x] Do a happy dance
* [x] Sleep

W8D3
* [x] Imaginary team did:
  * [x] in conference_go/urls.py added include for api_urls in accounts
  * [x] in monolith/api_urls.py import and register collection function view and single item functino view
  * [x] monolith/accounts/api_views all the functions and associated information
  * [x] migrate
  * [x] test

* [] Account Microservice
  * [x] Create the model named AccountVO for that will hold the data from the JSON above
  * [x] Add pika to the requirements.txt since you'll be using RabbitMQ
  * [x] Add a Dockerfile specifically for the new service
  * [x] Add the new service to the Docker Compose file
  * [x] Run the new Dockerfile in interactive mode so you can make and apply the migrations
  * [x] Write code to handle the new messages from the other team's code and insert or delete an AccountVO record
  * [x] Write code to include the "has_account" key in the detail views of the Attendee to indicate if an account exists based on if there's an AccountVO record with the same email
  * [x] test and sleep